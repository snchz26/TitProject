package com.tit.titproject.Activities;

import android.annotation.TargetApi;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.PermissionRequest;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.tit.titproject.R;


public class AnswerActivity extends AppCompatActivity {

    private WebView myWebView;
    private Boolean receiving = false;
    private MyWebChromeClient webChromeClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        receiving = true;
        setContentView(R.layout.activity_answer);
        myWebView = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.loadUrl("https://192.168.2.1:8080/demos/demo_audio_video_simple.html");
        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        });
        webChromeClient = new MyWebChromeClient();
        myWebView.setWebChromeClient(webChromeClient);
    }

    @Override
    protected void onPause() {
        super.onPause();
        myWebView.loadUrl("");
        receiving = false;
        myWebView.setWebChromeClient(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        myWebView.loadUrl("https://192.168.2.1:8080/demos/demo_audio_video_simple.html");
        receiving = true;
        myWebView.setWebChromeClient(webChromeClient);
    }

    @Override
    public void onBackPressed() {
        myWebView.loadUrl("javascript:androidHangUp()");
        super.onBackPressed();
        finish();
        receiving = false;
    }

    public class MyWebChromeClient extends WebChromeClient {
        @Override
        public void onPermissionRequest(final PermissionRequest request) {

            AnswerActivity.this.runOnUiThread(new Runnable() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {
                    // Make sure the request is coming from our file
                    // Warning: This check may fail for local files
                    if (receiving && request.getOrigin().toString().equals("https://192.168.2.1:8080/")) {
                        request.grant(request.getResources());
                    } else {
                        request.deny();
                    }
                }
            });
        }
    }

}

