package com.tit.titproject.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tit.titproject.R;


public class TreatmentFragment extends Fragment {

    public static Fragment newInstance() {
        return new TreatmentFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_treatment, null);
        return v;
    }
}
