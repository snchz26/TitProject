package com.tit.titproject.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.tit.titproject.R;


public class MainFragment extends Fragment {

    public static Fragment newInstance() {
        return new MainFragment();
    }

    private ImageView heart;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.main_fragment, null);
        heart = (ImageView) v.findViewById(R.id.heart);
        Animation anim = AnimationUtils.loadAnimation(getContext().getApplicationContext(), R.anim.heart_beating);
        heart.startAnimation(anim);
        return v;
    }

}
