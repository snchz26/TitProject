package com.tit.titproject.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tit.titproject.R;


public class VitalSignsFragment extends Fragment {

    public static Fragment newInstance() {
        return new VitalSignsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_vital_signs, null);
        return v;
    }
}
