package com.tit.titproject.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tit.titproject.R;


public class EmergencyFragment extends Fragment {
    public static EmergencyFragment newInstance() {
        return new EmergencyFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_emergency, null);
        return v;
    }
}
